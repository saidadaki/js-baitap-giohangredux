import { combineReducers } from "redux";
import { ShoeShopReducer } from "./ShoeShopReducer";

export const rootReducer_Shoe = combineReducers({ ShoeShopReducer });
