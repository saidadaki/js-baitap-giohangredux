import { shoeArr } from "../ShoeShop/ShoeData";

let initialState = {
  shoeArr: shoeArr,
  detailShoe: shoeArr[0],
  cart: [],
};

export const ShoeShopReducer = (state = initialState, action) => {
  switch (action.type) {
    case "ADD_SHOE": {
      let cloneCart = [...state.cart];
      let index = cloneCart.findIndex((item) => {
        return item.id == action.payload.id;
      });
      if (index == -1) {
        cloneCart.push({ ...action.payload, soLuong: 1 });
      } else {
        cloneCart[index].soLuong++;
      }
      return { ...state, cart: cloneCart };
    }
    case "GET_DETAIL": {
      return { ...state, detailShoe: action.payload };
    }
    case "DELETE_SHOE": {
      let cloneCart = [...state.cart];
      let index = cloneCart.findIndex((item) => {
        return item.id == action.payload;
      });
      cloneCart.splice(index, 1);
      return { ...state, cart: cloneCart };
    }
    case "CHANGE_QUANTITY": {
      let cloneCart = [...state.cart];
      let index = cloneCart.findIndex((item) => {
        return item.id == action.payload.id;
      });
      cloneCart[index].soLuong += action.payload.luaChon;
      cloneCart[index].soLuong == 0 && cloneCart.splice(index, 1);
      return { ...state, cart: cloneCart };
    }
    default: {
      return state;
    }
  }
};
