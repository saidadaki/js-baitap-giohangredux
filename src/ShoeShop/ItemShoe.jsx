import React, { Component } from "react";
import { connect } from "react-redux";

class ItemShoe extends Component {
  render() {
    return (
      <div className="col-6">
        <div
          className="card mx-auto my-1 text-center"
          style={{ width: "20rem" }}
        >
          <img className="card-img-top" src={this.props.dataShoe.image} />
          <div className="card-body">
            <h6 className="card-title">{this.props.dataShoe.description}</h6>
            <p className="card-text">{this.props.dataShoe.price}</p>
            <button
              onClick={() => {
                this.props.handleProductDetail(this.props.dataShoe);
              }}
              className="btn btn-warning mx-1"
              data-toggle="modal"
              data-target="#exampleModal"
            >
              View Detail
            </button>
            <button
              onClick={() => {
                this.props.handleAddToCartList(this.props.dataShoe);
              }}
              className="btn btn-info mx-1"
              data-toggle="modal"
              data-target="#exampleModalCart"
            >
              Add to cart
            </button>
          </div>
        </div>
      </div>
    );
  }
}

let mapDispatchToProps = (dispatch) => {
  return {
    handleAddToCartList: (shoe) => {
      let action = {
        type: "ADD_SHOE",
        payload: shoe,
      };
      dispatch(action);
    },
    handleProductDetail: (shoe) => {
      let action = {
        type: "GET_DETAIL",
        payload: shoe,
      };
      dispatch(action);
    },
  };
};
export default connect(null, mapDispatchToProps)(ItemShoe);
