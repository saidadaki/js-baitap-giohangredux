import React, { Component } from "react";
import { connect } from "react-redux";
import ItemShoe from "./ItemShoe";

class ListShoe extends Component {
  renderListShoe = () => {
    return this.props.shoeArr.map((item, index) => {
      return (
        <ItemShoe
          handleProductDetail={this.props.handleViewShoeDetail}
          handleAddToCartList={this.props.handleAddToCartList}
          key={index}
          dataShoe={item}
        />
      );
    });
  };
  render() {
    return <div className="row">{this.renderListShoe()}</div>;
  }
}

let mapStateToProps = (state) => {
  return {
    shoeArr: state.ShoeShopReducer.shoeArr,
  };
};
export default connect(mapStateToProps)(ListShoe);
