import React, { Component } from "react";
import CartShoe from "./CartShoe";
import DetailShoe from "./DetailShoe";

import ListShoe from "./ListShoe";
import { shoeArr } from "./ShoeData";

export default class ShoeShop extends Component {
  render() {
    return (
      <div>
        <nav className="bg-dark text-center mx-auto text-light py-3 h2 ">
          ONLINE SHOE SHOP
        </nav>
        <ListShoe />
        <CartShoe />
        <DetailShoe />
      </div>
    );
  }
}
